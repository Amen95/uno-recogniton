import cv2
import numpy as np
from tensorflow.keras.models import load_model
import tensorflow as tf
import tensorflow.keras.backend as K
import logging

# Disable TensorFlow warnings
logging.getLogger('tensorflow').setLevel(logging.ERROR)

# I have defined both the swish function and fixeddropout layers because they were used internally in the pre-trained model. I had to include them in the code to avoid getting the deserialization error when loading the saved model.
def swish(x):
    return x * K.sigmoid(x)
# Register Swish as a custom object
tf.keras.utils.get_custom_objects()['swish'] = swish

# Defining the custom layer because it was used internally on the pre trained effientnet B1 model so had to includ it here
class FixedDropout(tf.keras.layers.Layer):
    def __init__(self, rate, seed=None, noise_shape=None, **kwargs):
        super(FixedDropout, self).__init__(**kwargs)
        self.rate = rate
        self.seed = seed
        self.noise_shape = noise_shape

    def call(self, inputs, training=None):
        if training:
            return tf.nn.dropout(inputs, rate=self.rate, seed=self.seed, noise_shape=self.noise_shape)
        return inputs

    def get_config(self):
        config = super(FixedDropout, self).get_config()
        config.update({'rate': self.rate, 'seed': self.seed, 'noise_shape': self.noise_shape})
        return config
# Register the custom layer
tf.keras.utils.get_custom_objects()['FixedDropout'] = FixedDropout

# Load the CNN model
CNN_Model = load_model('Intelligent Sensing Coursework\\CNN Extraction\\Classifier_Model.keras') # adjust the path as needed

# Define the class mapping dictionary
class_mapping = {
    0: 'blue 0', 1: 'blue 1', 2: 'blue 2', 3: 'blue 3', 4: 'blue 4', 5: 'blue 5', 6: 'blue 6', 7: 'blue 7', 8: 'blue 8', 9: 'blue 9',
    10: 'blue draw', 11: 'blue reverse', 12: 'blue skip',
    13: 'green 0', 14: 'green 1', 15: 'green 2', 16: 'green 3', 17: 'green 4', 18: 'green 5', 19: 'green 6', 20: 'green 7', 21: 'green 8', 22: 'green 9',
    23: 'green draw', 24: 'green reverse', 25: 'green skip',
    26: 'red 0', 27: 'red 1', 28: 'red 2', 29: 'red 3', 30: 'red 4', 31: 'red 5', 32: 'red 6', 33: 'red 7', 34: 'red 8', 35: 'red 9',
    36: 'red draw', 37: 'red reverse', 38: 'red skip',
    39: 'wild +2', 40: 'wild +4',
    41: 'yellow 0', 42: 'yellow 1', 43: 'yellow 2', 44: 'yellow 3', 45: 'yellow 4', 46: 'yellow 5', 47: 'yellow 6', 48: 'yellow 7', 49: 'yellow 8', 50: 'yellow 9',
    51: 'yellow draw', 52: 'yellow reverse', 53: 'yellow skip'
}

# Function to perform image classification using the CNN model
def classify_image(img):
    # Preprocess the image for the CNN model
    img = cv2.resize(img, (224, 224))  # Assuming input size of the CNN model is (224, 224)
    img = img / 255.0  # Normalize pixel values
    
    # Performing prediction using the CNN model
    prediction = CNN_Model.predict(np.expand_dims(img, axis=0))
    
    # Getting the predicted class index
    predicted_class_index = np.argmax(prediction)
    
    # Returning the predicted class index
    if predicted_class_index in [39, 40]:  # Check for wild +2 and wild +4
        return class_mapping[predicted_class_index]  # Return the full class name for specified cards 
    else:
        # Extracting and returning the shape part from the class name
        shape_part = class_mapping[predicted_class_index].split(' ')[-1]  # Split by space and get the last part only excluding the colors part
        return shape_part

# Function to detect colors in the image
def detect_colors(img):
    # Converting frame to HSV color space
    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    
    # Defining color ranges
    colors = {
        "Blue": ([90, 100, 50], [120, 255, 255]),   # Define the lower and upper bounds for blue
        "Green": ([80, 100, 50], [100, 255, 255]),   # Define the lower and upper bounds for green
        "Yellow": ([20, 50, 100], [35, 255, 255]),   # Define the lower and upper bounds for yellow
        "Red": ([145, 50, 100], [180, 255, 255]),   # Define the lower and upper bounds for red
    }
    
    # Set to store detected colors
    detected_colors = set()
    
    # Iterating over colors
    for color_name, (lower, upper) in colors.items():
        # Thresholding the blurred HSV image to get only specified colors
        mask = cv2.inRange(hsv, np.array(lower), np.array(upper))
        
        # Applying morphological operations to remove noise specially mixed colors 
        kernel = np.ones((9, 9), np.uint8)
        mask = cv2.morphologyEx(mask, cv2.MORPH_OPEN, kernel)
        mask = cv2.morphologyEx(mask, cv2.MORPH_CLOSE, kernel)
        
        # Finding contours
        contours, _ = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        
        # Filtering contours based on area
        for contour in contours:
            area = cv2.contourArea(contour)
            if area > 1000:  # Adjust the threshold value as needed
                detected_colors.add(color_name)
                break  # Stop processing contours for this color if one is detected
    
    # Preparing text to display only detected region and color will be displayed as text once instead of being displayed multiple times for each detected color.
    color_text = "Color: " + ", ".join(detected_colors) if detected_colors else "Color:" # it will only display color if detected.
    
    # Draw color text on the frame
    cv2.putText(img, color_text, (10, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (150, 250, 150), 3)

# Open video capture
cap = cv2.VideoCapture(1)
while True:
    # Capture frame-by-frame
    ret, frame = cap.read()
    
    # Create a copy of the frame for color detection
    color_detection_frame = frame.copy()
    
    # Detect colors in the frame
    detect_colors(color_detection_frame)
    
    # Perform image classification using the CNN model
    shape_part = classify_image(frame)
    
    # Display the detected shape alongside the color
    cv2.putText(frame, "Shape: " + shape_part, (10, 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (150, 250, 150), 3) # shapes will be displayed if detected
    
    # Display the resulting frame with both CNN results and HSV results
    cv2.imshow('Image Classification', frame)
    cv2.imshow('HSV Color Detection', color_detection_frame)
    
    # Exit loop if 'q' is pressed
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# Release video capture
cap.release()
cv2.destroyAllWindows()
