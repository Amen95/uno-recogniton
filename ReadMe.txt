Get Started

Prerequisites
To run this task, you will need:
• Python 3.7 or higher
• OpenCV
• TensorFlow 2.x
• Keras
• Numpy
• Matplotlib
• Joblib
• Tkinter
• sklearn
• RandomForest

Installation
• Install Python 3.7 or higher.
• Install OpenCV, Matplotlib, Numpy, TensorFlow, and Keras using pip:

pip install opencv-python matplotlib numpy tensorflow keras
pip install joblib
pip install scikit-learn

Running the Codes for CNN.py, CNN + HSV.py, Features_Extractor.py files
• Install the required packages by running the command pip install -r requirements.txt.
• Open Terminal, navigate to project directory and run CNN + HSV.py file.
• Dataset inside Features Extraction folder are used for training using RandomForest (RF) Model.
• Run RF.py python file and load image using the GUI extractor implemented, try loading the images from "Samples" file inside Features Extraction directory.
• Navigate to CNN_UNO.ipynb Notebook where training phase and CNN only prediction code are presented.
• Saved models for CNN and joblib are included on each corresponding file directory.
• Capture Uno card images from live camera stream: This option opens the live camera stream to capture the Uno cards used by CNN and CNN + HSV.
• The employed GUI for Features_Extraction offers two options: either by predicting from browsing images or predicting from live feed.
